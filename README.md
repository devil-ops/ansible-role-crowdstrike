# Duke Crowdstrike

Required Vars

```
crowdstrike_cid="CID_FROM_DEVICE_FOLKS"
```

Internal IP addresses don't need a username password to authenticate against
the repos, however if you need to run this from outside Duke, you can use some
additional vars below:

```
crowdstrike_external_repo=true
crowdstrike_username=duke 
crowdstrike_password='PASSWORD_FROM_DEVICE_FOLKS' 
```
